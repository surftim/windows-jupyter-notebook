 
$LOGFILE = "c:\logs\plugin-windows-jupyter-notebook.log"

Function Write-Log([String] $logText) {
  '{0:u}: {1}' -f (Get-Date), $logText | Out-File $LOGFILE -Append
}

Function Main {

  Write-Log "Start windows-jupyter-notebook"
 

  Write-Log "Install python"

  try {

    choco feature enable -n allowGlobalConfirmation

    If(Test-Path -Path "$env:ProgramData\Chocolatey") {
      choco install python3 --override --no-progress
      $Env:PATH += ";c:\Python311;c:\Python311\Scripts"
    }
    Else {
      Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
      choco install python3 --override --no-progress
      $Env:PATH += ";c:\Python311;c:\Python311\Scripts"
    }
  }
  catch {
      Write-Log "$_"
      Throw $_
  }

  Write-Log "Install jupyterlab"
  try {
    python.exe -m pip install --upgrade pip
    pip install jupyterlab
    pip install pandas
   

  }
  catch {
      Write-Log "$_"
      Throw $_
  }

  Write-Log 'Create service'
  try {
    If(Test-Path -Path "$env:ProgramData\Chocolatey") {
      choco install nssm -y --override
    }
    Else {
      Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
      choco install nssm -y --override
    }
  }
  catch {
      Write-Log "$_"
      Throw $_
  }

  try {
  nssm install JupyterLab jupyter-lab --NotebookApp.token='' --NotebookApp.password=''

  Set-Service -Name JupyterLab -StartupType Automatic
  Set-Service -Name JupyterLab -Status Running
  }
    catch {
      Write-Log "$_"
      Throw $_
  }

  Write-Log 'Create shortcuts'

  $shortcut = 'C:\Users\Public\Desktop\JupyterLab.lnk'
  $shortcut_target = 'http://localhost:8888/lab'


  $WebClient = New-Object System.Net.WebClient
  $WebClient.DownloadFile("https://jupyter.org/favicon.ico","c:\Python311\Scripts\jupyter.ico")
  $iconLocation = 'c:\Python311\Scripts\jupyter.ico'
  
  Import-Module "$env:ChocolateyInstall\helpers\chocolateyInstaller.psm1"; Install-ChocolateyShortcut -ShortcutFilePath $shortcut -TargetPath $shortcut_target -IconLocation $iconLocation

  
  Write-Log "End windows-jupyter-notebook"
 

}

Main     
